import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const users = new UsersController();
xdescribe("Token usage", () => {
    let accessToken: string;
    before(`Login and get the token`, async () => {
        let response = await auth.login("usr2@gmail.com", "password2");
            accessToken = Response.body.token.accessToken.token;
        // console.log(accessToken);
    });
    it(`Usage is here`, async () => {
        let userData: object = {
            id: 2434,
            avatar: "string",
            email: "usr2@gmail.com",
            userName: "User2",
        };

        let response = await users.updateUser(userData, accessToken);
        expect(response.statusCode, `Status Code should be 204`).to.be.equal(201);
    });
});